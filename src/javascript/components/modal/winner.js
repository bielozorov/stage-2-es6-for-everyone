import {showModal} from "./modal";

export function showWinnerModal(fighter) {
    // call showModal function
    let winnerData = document.createElement('div');
    winnerData.classList.add('winner-data');
    let winnerImage = document.createElement('img');
    winnerImage.classList.add('winner-image');
    winnerImage.src = fighter.source;
    let winnerIndexes = document.createElement('p');
    winnerIndexes.classList.add('winner-indexes');
    winnerIndexes.innerHTML = `
    <p> id: ${fighter._id};</p>
    <p> health: ${fighter.health};</p>
    <p> attack: ${fighter.attack};</p>
    <p> defense: ${fighter.defense}; </p>`;

    winnerData.append(winnerImage, winnerIndexes);

    console.log(fighter);
    const winnerInfo = {
        title: 'Winner is ' + fighter.name,
        bodyElement: winnerData
    }
    showModal(winnerInfo);
}