import {createElement} from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
    let {source, name, health, attack, defense} = fighter;
    const positionClassName = position === 'right' ? 'fighter-preview___right fighter-preview' : 'fighter-preview___left fighter-preview';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    let preview = document.createElement("div");
    preview.innerHTML = `<img src="${source}" alt="" width="170" height="200">
                <p style="background-color: moccasin;">Name: ${name} </p>
                <p style="background-color:moccasin;">Health: ${health} </p>
                <p style="background-color:moccasin;">Attack: ${attack}</p>
                <p style="background-color:moccasin;">Defence: ${defense}</p>`;
    fighterElement.append(preview);
    return fighterElement;
}

function createFighter(fighter, position) {
    const imgElement = createFighterImage(fighter);
    const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
    const fighterElement = createElement({
        tagName: 'div',
        className: `arena___fighter ${positionClassName}`,
    });

    fighterElement.append(imgElement);
    return fighterElement;
}

export function createFighterImage(fighter) {
    const {source, name} = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });
    return imgElement;
}